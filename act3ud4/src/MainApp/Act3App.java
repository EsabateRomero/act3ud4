/**
 * 
 */
package MainApp;

/**
 * @author Elisabet Sabat�
 *
 */
public class Act3App {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
	
		int x=10, y=5;
		double n=10.0, m=5.0;
				/*Todas estas operaciones pueden ponere en el sout a partir de una variable
		 *  pero como nuestro �nico objetivo es solo ense�ar como estas interactuan 
		 *  entre si no veo necesario la creaci�n de dicha variable y voy a optar por
		 *  hacer las operaciones directamente ya que hay muchos apartados en este ejercicio*/
		
		
		
		System.out.println("1. Valor de cada variable");
		System.out.println("x = "+x+" / y = "+y+" / n = "+n+" / m = "+ m );
		System.out.println("---------------------------------------------");
		// OPERACIONES ENTRE ENTEROS
		System.out.println("2. La suma");
		System.out.println("X + Y = " + (x+y));
		
		System.out.println("3. La diferencia");
		System.out.println("X - Y = " + (x-y));
		
		System.out.println("4. El producto");
		System.out.println("X * Y = " + (x*y));
		
		System.out.println("5. El cociente");
		System.out.println("X / Y = " + (x/y));
		
		System.out.println("6. El resto");
		System.out.println("X % Y = " + (x%y));
		
		System.out.println("---------------------------------------------");
		// OPERACIONES COMA FLOTANTE
		System.out.println("7. La suma");
		System.out.println("M + N = " + (m+n));
		
		System.out.println("8. La diferencia");
		System.out.println("M - N = " + (m-n));
		
		System.out.println("9. El producto");
		System.out.println("M * N = " + (m*n));
		
		System.out.println("10. El cociente");
		System.out.println("M / N = " + (m/n));
		
		System.out.println("11. El resto");
		System.out.println("M % N = " + (m%n));
		
		System.out.println("---------------------------------------------");
		// OPERACIONES CON ENTEROS Y COMA FLOTANTE
		System.out.println("12. La Suma");
		System.out.println("X + N = " + (x+n));
		
		System.out.println("13. El cociente");
		System.out.println("Y / M = " + (y/m));
		
		System.out.println("14. El resto");
		System.out.println("Y % M = " + (y%m));
		
		System.out.println("15. El doble de cada variable");
		System.out.println("Doble de X = " + 2*x);
		System.out.println("Doble de Y = " + 2*y);
		System.out.println("Doble de M = " + 2*m);
		System.out.println("Doble de N = " + 2*n);
		
		System.out.println("16. La suma de todas las variables");
		System.out.println("X + Y + M + N = " + (x+y+m+n));
		
		System.out.println("17. El producto de todas las variables");
		System.out.println("X * Y * M * N = " + (x*y*m*n));
		
	}

}
